public abstract class Shape {
    private String name;
    protected Shape(String name){
        this.name = name;
    }
    public String getNeme(){
        return name;
    }
    public abstract double calArea();

    public abstract double calPerimeter();
}
